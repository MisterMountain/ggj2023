# GGJ 2023

this is the git repo of the Game "Thirsty Roots" for the Global Game Jam in Ansbach 2023 with the topic roots.
The Game uses the Unity game engine, so cloning the repository and the opening it in Unity 2021.3.18f1 should be enough to develop.
You play as a root seeking its way to water, in order to win the game you need to find 3 water buckets in time. If you do not, you will get dried.
