using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;

public class Player : MonoBehaviour
{
    private const float borderUp = 3;
    private const float borderRight = 5;
    private const float borderLeft = -5;
    private float speed = 1f;
    private Transform _transformY;
    private Transform _transformX;
    private Vector2 _currentPosY;
    private Vector2 _currentPosX;
    public int score =0;

    private Vector3 Ziel1 = new Vector3(-16.5f, -16.5f, 0);
    private Vector3 Ziel2 = new Vector3(2.5f, -15.5f, 0);
    private Vector3 Ziel3 = new Vector3(14.5f, -16.5f, 0);

    // WurzelEnde
    public Tile wurzelEndeTileRight;
    public Tile wurzelEndeTileDown;
    public Tile wurzelEndeTileLeft;

    //WurzelKurvenEnden
    public Tile wurzelEndeUpperRight;
    public Tile wurzelEndeUpperLeft;
    public Tile wurzelEndeLowerRight;
    public Tile wurzelEndeLowerLeft;

    // WurzelTeile
    public Tile wurzelTileHorizontal;
    public Tile wurzelTileVertical;

    // WurzelKurven
    public Tile wurzelUpperRight;
    public Tile wurzelUpperLeft;
    public Tile wurzelLowerRight;
    public Tile wurzelLowerLeft;

    // pre
    public Vector3Int pre;
    public Vector3Int prepre;
    public int directionPre;
    public int directionPrePre;
    public bool endCurve;

    //Wurzelmap
    public Tilemap WurzelMap;

   // public GameObject prefab; // Hier im Editor dein Prefab reinziehen
    UnityEvent moveEventRight = new UnityEvent();
    UnityEvent moveEventDown = new UnityEvent();
    UnityEvent moveEventLeft = new UnityEvent();


    void Start () {
        _transformY = gameObject.GetComponent<Transform> ();
        _currentPosY = _transformY.position;        

        _transformX = gameObject.GetComponent<Transform> ();
        _currentPosX = _transformX.position;

        //Add a listener to the new Event. Calls MyAction method when invoked
        moveEventRight.AddListener(moveRight);
        moveEventDown.AddListener(moveDown);
        moveEventLeft.AddListener(moveLeft);

        
        pre = WurzelMap.WorldToCell(transform.position);
        prepre = WurzelMap.WorldToCell(transform.position);

        directionPre = 2;
        directionPrePre = 2;
        endCurve = false;
	 	score = 0;
    }

    void setAnfang()
    {
        pre = WurzelMap.WorldToCell(transform.position);
        prepre = WurzelMap.WorldToCell(transform.position);

        directionPre = 2;
        directionPrePre = 2;
        endCurve = false;
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("d") && moveEventRight != null){
            moveEventRight.Invoke();
        }
        if (Input.GetKeyDown("s") && moveEventDown != null){
            moveEventDown.Invoke();
        }
        if (Input.GetKeyDown("a") && moveEventLeft != null){
            moveEventLeft.Invoke();
        }
    }

    //Movement Statev 1 = Right / 2 = Down / 3 = Left
    void moveRight(){
       
            
		Vector3 tmp = new Vector3(transform.position.x + speed, transform.position.y, transform.position.z);
		if(tmp ==  Ziel1 || tmp == Ziel2 || tmp == Ziel3){
            score++;

            
            switch(score){
                case 1: transform.position = new Vector3(0.5f, -0.5f,0);
                setAnfang();
                break;
                case 2: transform.position = new Vector3(2.5f, -0.5f,0);
                setAnfang();
                break;
                case 3: UnityEngine.SceneManagement.SceneManager.LoadScene("WinScreen"); //win
                break;
            }
        }
        
        if (!WurzelMap.GetTile(WurzelMap.WorldToCell(tmp))){
		ReplaceEndWithOpen();

            transform.position = new Vector2 (transform.position.x + speed, transform.position.y);

            switch (directionPre)
            {
                case 1: // Right -> Right
                    //WurzelMap.SetTile(prepre, wurzelTileHorizontal);
                    WurzelMap.SetTile(pre, wurzelEndeTileRight);
                    
                    break;
                case 2: // Down -> Right
                    WurzelMap.SetTile(pre, wurzelEndeUpperRight);
                    endCurve = true;
                    break;
            }
            directionPrePre = directionPre;
            directionPre = 1;

            prepre = pre;
            pre = WurzelMap.WorldToCell(transform.position);
        }
    }

    void moveDown(){

            

		Vector3 tmp = new Vector3(transform.position.x, transform.position.y - speed, transform.position.z);

        if(tmp ==  Ziel1 || tmp == Ziel2 || tmp == Ziel3){
            score++;
            
            switch(score){
                case 1: transform.position = new Vector3(0.5f, -0.5f,0);
                setAnfang();
                break;
                case 2: transform.position = new Vector3(2.5f, -0.5f,0);
                setAnfang();
                break;
                case 3: UnityEngine.SceneManagement.SceneManager.LoadScene("WinScreen"); //win
                break;
            }
        }

		if(!WurzelMap.GetTile(WurzelMap.WorldToCell(tmp))){
        
		ReplaceEndWithOpen();

        transform.position = new Vector2 (transform.position.x, transform.position.y - speed);
        
        switch (directionPre)
        {
            case 1: // Right -> Down
                WurzelMap.SetTile(pre, wurzelEndeLowerLeft);
                endCurve = true;
                break;
            case 2: // Down -> Down
                //WurzelMap.SetTile(prepre, wurzelTileVertical);
                WurzelMap.SetTile(pre, wurzelEndeTileDown);
                break;
            case 3: // Left -> Down
                WurzelMap.SetTile(pre, wurzelEndeLowerRight);
                endCurve = true;
                break;
        }
        

        directionPrePre = directionPre;
        directionPre = 2;
        prepre = pre;
        pre = WurzelMap.WorldToCell(transform.position);
	}
    }
    
    void moveLeft(){

            
		
		Vector3 tmp = new Vector3(transform.position.x - speed, transform.position.y, transform.position.z);

        if(tmp ==  Ziel1 || tmp == Ziel2 || tmp == Ziel3){
            score++;
            
            switch(score){
                case 1: transform.position = new Vector3(0.5f, -0.5f,0);
                setAnfang();
                break;
                case 2: transform.position = new Vector3(2.5f, -0.5f,0);
                setAnfang();
                break;
                case 3: UnityEngine.SceneManagement.SceneManager.LoadScene("WinScreen"); //win
                break;
            }
        }

       	 if (!WurzelMap.GetTile(WurzelMap.WorldToCell(tmp))){
            ReplaceEndWithOpen();
            
            transform.position = new Vector2 (transform.position.x - speed, transform.position.y);

            switch (directionPre)
            {
                case 2: // Down -> Left
                    WurzelMap.SetTile(pre, wurzelEndeUpperLeft);
                    endCurve = true;
                    break;
                case 3: // Left -> Left

                    //WurzelMap.SetTile(prepre, wurzelTileHorizontal);
                    WurzelMap.SetTile(pre, wurzelEndeTileLeft);
                    break;
            }
            directionPrePre = directionPre;
            directionPre = 3;

            prepre = pre;
            pre = WurzelMap.WorldToCell(transform.position);
        }
    }

    void ReplaceEndWithOpen(){
        if(endCurve){
            switch(directionPrePre){
                case 2:
                if(directionPre == 1){               
                    WurzelMap.SetTile(prepre, wurzelUpperRight);
                }else {

                    WurzelMap.SetTile(prepre, wurzelUpperLeft);
                }
                break;
                case 1:
                if(directionPre == 1){

                    WurzelMap.SetTile(prepre, wurzelLowerRight);
                }else {
                    WurzelMap.SetTile(prepre, wurzelLowerLeft);
                }
                break;
                case 3:
                if(directionPre == 1){

                    WurzelMap.SetTile(prepre, wurzelLowerRight);
                }else {
                    WurzelMap.SetTile(prepre, wurzelLowerRight);
                }
                break;
                default: break;
            }
            endCurve = false;
        }
        else{
            if(directionPrePre == 2){
                WurzelMap.SetTile(prepre, wurzelTileVertical);
            }else{
                WurzelMap.SetTile(prepre, wurzelTileHorizontal);
            }
        }
    }
}
