using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    // Available time to beat the level in seconds
    public int iSeconds = 500;

    // Time-Text for display
    public Text timeText;

    // Internal timer counting down
    private float fTimeRemaining;

    // Start is called once
    void Start()
    {
      fTimeRemaining = iSeconds;
    }

    // Update is called once per frame
    void Update()
    {
        if (fTimeRemaining > 0)
        {
          fTimeRemaining -= Time.deltaTime;
          DisplayTimer(fTimeRemaining);
        }
        else
        {
          UnityEngine.SceneManagement.SceneManager.LoadScene("DriedScreen");
        }
    }

    void DisplayTimer(float fTime)
    {
        int iMins = Mathf.FloorToInt(fTime / 60);
        iMins = iMins < 0 ? 0 : iMins;
        int iSecs = Mathf.FloorToInt(fTime % 60);
        iSecs = iSecs < 0 ? 0 : iSecs;
        timeText.text = string.Format("{0:00}:{1:00}", iMins, iSecs);
    }
}
