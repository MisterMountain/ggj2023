using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scroller : MonoBehaviour
{
    public Transform credits;

    void Start() 
    {
    }

    // Update is called once per frame
    void Update()
    {
        credits.position = new Vector2(credits.position.x, credits.position.y - 1f);
        if (credits.position.y < -215)
        {
            credits.position = new Vector2(600f, 700f);
        }
    }
}
